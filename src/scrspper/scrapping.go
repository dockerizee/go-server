package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var scrapBaseURL = "https://jsonmock.hackerrank.com/api/countries"

type ApiResponse struct {
	Page       int           `json:"page"`
	TotalPages int           `json:"total_pages"`
	Data       []CountryInfo `json:"data"`
}

type CountryInfo struct {
	Name       string `json:"name"`
	Alpha2Code string `json:"alpha2Code"`
	Alpha3Code string `json:"alpha3Code"`
}

// info of scrapped results
type ScrapResult struct {
	TotalPages  int
	Page        int // page found at
	Key         string
	CountryName string //name of country
}

func Scrap(countryCode string) *ScrapResult {
	res, found := scrapNext(countryCode)
	if found {
		return res
	}
	channel := make(chan ScrapResult)
	channelDone := make(chan bool)
	for i := 2; i <= res.TotalPages; i++ {
		go scrapNextAsync(channel, channelDone, countryCode, i)
	}

	sentinel := res.TotalPages - 1
	for i := 0; i < sentinel; {
		select {
		case x := <-channel:
			return &x
		case <-channelDone:
			i++

		}
	}
	return nil

}

func scrapNextAsync(ch chan ScrapResult, complete chan bool, countryCode string, page int) {
	defer func() {
		complete <- true
	}()
	res, found := scrapNext(countryCode, page)
	if found {
		ch <- *res
	}
}

func scrapNext(countryCode string, page ...int) (*ScrapResult, bool) {
	var (
		query       map[string]interface{}
		apiResponse ApiResponse
	)
	if len(page) != 0 {
		query = map[string]interface{}{
			"page": page[0],
		}
	}
	if err := httpGet(scrapBaseURL, query, &apiResponse); err != nil {
		return nil, false
	}

	for _, tmp := range apiResponse.Data {
		if tmp.Alpha2Code == countryCode || tmp.Alpha3Code == countryCode {
			return &ScrapResult{
				Page:        apiResponse.Page,
				TotalPages:  apiResponse.TotalPages,
				Key:         countryCode,
				CountryName: tmp.Name,
			}, true
		}

	}

	return &ScrapResult{
		Page:       apiResponse.Page,
		TotalPages: apiResponse.TotalPages,
	}, false
}

func httpGet(baseURL string, query map[string]interface{}, result interface{}) (err error) {
	var (
		client = http.Client{}
		req    *http.Request
		resp   *http.Response
		body   []byte
	)
	if req, err = http.NewRequest("GET", baseURL, nil); err != nil {
		return err
	}

	q := req.URL.Query()
	for k, v := range query {
		tmp := fmt.Sprintf("%v", v)
		q.Add(k, tmp)
	}
	req.URL.RawQuery = q.Encode()

	if resp, err = client.Do(req); err != nil {
		return err
	}
	defer resp.Body.Close()

	if body, err = ioutil.ReadAll(resp.Body); err != nil {
		return
	}

	return json.Unmarshal(body, result)
}
