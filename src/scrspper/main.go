package main

import "fmt"

func main() {
	values := []string{
		"AF", "AR", "KH", "0", "URY",
	}

	for _, v := range values {
		if res := Scrap(v); res != nil {
			fmt.Printf("CountryCode[%v] found at page{%v}: %v\n", v, res.Page, res.CountryName)
			continue
		}
		fmt.Println(v, "not found")
	}
}
