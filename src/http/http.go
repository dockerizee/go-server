package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
)

type H struct {
	Cases map[string]*Payload
}

type Payload struct {
	StatusCode int
	Payload    interface{}
}

func NewTestServerBuilder() *H {
	return &H{
		Cases: map[string]*Payload{
			"/ping": &Payload{
				StatusCode: 200,
			},
			"/info": &Payload{
				StatusCode: 200,
				Payload: struct {
					Name string `json:"name"`
				}{Name: "Random"},
			},
		},
	}
}

func (h *H) Server() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := h.Cases[r.URL.Path]
		respBody, _ := json.Marshal(response.Payload)
		w.WriteHeader(response.StatusCode)
		w.Write(respBody)
	}))

}

func main() {
	fmt.Println("start")
	s := NewTestServerBuilder().Server()
	client := s.Client()
	res, err := client.Get(s.URL + "/info?q=me")
	if err != nil {
		fmt.Println("get err", err)
		return
	}

	fmt.Println("res")

	greeting, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		fmt.Println("body err", err)
		return
	}
	fmt.Println("b", string(greeting))

	fmt.Printf("%s", greeting)

}
