package src

import (
	"fmt"
	"io/ioutil"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

type entry struct {
	ID        string    `db:"id"`
	Filebyte  []byte    `db:"content"`
	Timestamp time.Time `db:"timestamp"`
}

func Test_Stuff(t *testing.T) {
	var e entry
	db, err := sqlx.Open("mysql", "root:password@tcp(127.0.0.1:3308)/temp_db?parseTime=true")
	assert.NotNil(t, db)
	assert.Nil(t, err)

	b, err := ioutil.ReadFile("sk")
	assert.Nil(t, err)
	q := `INSERT INTO person (content) VALUES (?)`
	res, err := db.Exec(q, b)
	assert.Nil(t, err)

	lastID, _ := res.LastInsertId()
	id := fmt.Sprintln(lastID)
	fmt.Println("lastID", id)

	fmt.Println("db get")
	err = db.Get(&e, "SELECT * FROM person where id=?", id)
	assert.Nil(t, err)
	fmt.Println("e", e)
	assert.Equal(t, b, e.Filebyte)
}
