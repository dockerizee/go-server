package examplemiddleware

import (
	"log"
	"net/http"
)

// Logger logger middleware
type Logger struct {
}

// Middleware ...
func (me *Logger) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here
		log.Println(r.RequestURI)
		log.Println(r.Method)
		log.Println(r.Header)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}
