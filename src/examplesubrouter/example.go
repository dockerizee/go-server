package examplesubrouter

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"regexp"

	"github.com/gorilla/mux"
)

var (
	x map[string]func(w http.ResponseWriter, r *http.Request)
)

func custommw(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("middleware", r.URL)
		h.ServeHTTP(w, r)
	})
}

// AttachSubRouter attaches a subrouter to the root router
func AttachSubRouter(root *mux.Router, tag string) error {
	var validID = regexp.MustCompile(`/(.+)?`)
	if !validID.MatchString(tag) {
		fmt.Println("invalid tag")
		return errors.New("invalid tag")
	}
	fmt.Println("attaching subrouter:", tag)

	router := root.PathPrefix(tag).Subrouter()

	router.HandleFunc("/display/page", displayPage)
	router.HandleFunc("/{id}", productHandler)
	router.HandleFunc("", productsHandler)

	// attach custom middleware to speficif path
	router.Handle("/", custommw(http.HandlerFunc(productsHandler)))

	return nil
}

func displayPage(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		tpl := `<html>
	<head>
		<script>
		document.addEventListener("DOMContentLoaded", function(event) {
            document.createElement('form').submit.call(document.getElementById('member_signup'));
            });     
		</script>
		</head>
		<body>
		<form method="POST" action="" id="member_signup">
		<input type=hidden name="data" value="yourdata">
		</form>
		</body>
	HELLO
	</html>
	`
		t, _ := template.New("web").Parse(tpl)
		t.Execute(w, nil)
	case "POST":
		r.ParseForm()
		m := map[string]interface{}{}
		for key, val := range r.Form {
			v := val[0]
			m[key] = v
		}
		jsn, _ := json.Marshal(m)
		w.Write(jsn)
		w.WriteHeader(200)
	}

	// w.Header().Set("Content-Type", "text/html")
	// t.ExecuteTemplate(w, "T", nil)
	// w.WriteHeader(200)

}

func productHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	switch r.Method {
	case http.MethodGet:
		fmt.Println("call productHandler: GET BY ID: ", id)
	case http.MethodPut:
		fmt.Println("call productHandler: UPDATE BY ID: ", id)
	}

	w.WriteHeader(http.StatusOK)

}
func productsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		fmt.Println("call productsHandler: list products")
	case http.MethodPost:
		fmt.Println("call productsHandler: create product")
	}

	w.WriteHeader(http.StatusOK)
}
