FROM golang:1.15-alpine3.12 AS builder

WORKDIR /build 

COPY . .

RUN go mod download 

RUN go build -o main .

WORKDIR /dist 

RUN cp /build/main .

FROM alpine:latest

WORKDIR /app

COPY --from=builder /dist/main .

CMD ["/app/main"]

#reference https://tutorialedge.net/golang/go-docker-tutorial/