package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/dockerizee/go-server/src/examplemiddleware"
	"gitlab.com/dockerizee/go-server/src/examplesubrouter"
)

func init() {
	//initializers all here
}

func main() {
	var wait time.Duration
	r := mux.NewRouter()
	logger := (examplemiddleware.Logger{})
	r.Use(logger.Middleware)
	examplesubrouter.AttachSubRouter(r, "/product")

	srv := &http.Server{
		Addr:         ":8080",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			panic(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	srv.Shutdown(ctx)
	log.Println("shutting down")
	os.Exit(0)
}
