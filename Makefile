AUTHOR := "sabrinasays"
PROJECT:= "go-server"
EXPOSTPORT:= 8888

.PHONY: run

build: dep
	go build -o main .

dep:
	go mod download 
clean:
	rm main
	
run:
	go run *.go

docker-build:
	docker build . -t ${AUTHOR}/${PROJECT}

docker-run: 
    docker run -p ${EXPOSTPORT}:8080 -d ${AUTHOR}/${PROJECT}
	docker image prune --filter label=stage=builder
